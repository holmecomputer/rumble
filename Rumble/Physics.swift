//
//  Physics.swift
//  Rumble
//
//  Created by Sam DuBois on 7/16/18.
//  Copyright © 2018 Joshua Holme. All rights reserved.
//

import Foundation

struct PhysicsCategory
{
    static let character : UInt32 = 0x1 << 1
    static let ground : UInt32 = 0x1 << 2
}

