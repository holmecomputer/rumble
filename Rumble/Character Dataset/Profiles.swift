//
//  Profiles.swift
//  Rumble
//
//  Created by Sam DuBois on 7/16/18.
//  Copyright © 2018 Joshua Holme. All rights reserved.
//

import Foundation

// This class is utilizied to define and specify each character in the game and carry their attributes with them
class Profiles: Player {
    
    static let instance = Profiles()
    
    func Crystal() -> Player {
        let crystal = Player(imageNamed: "CrystalVector.pdf")
        crystal.PlayerXScale = 0.25
        crystal.PlayerYScale = 0.25
        return crystal
    }
    
    func MoneyBags() -> Player {
        let moneybags = Player(imageNamed: "MoneybagsVector.pdf")
        moneybags.PlayerXScale = 0.05
        moneybags.PlayerYScale = 0.05
        return moneybags
    }
    
    func Astro() -> Player {
        let astro = Player(imageNamed: "AstroVector.pdf")
        astro.PlayerXScale = 0.20
        astro.PlayerYScale = 0.20
        return astro
    }
    
    
}
