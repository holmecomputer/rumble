//
//  Player.swift
//  Rumble
//
//  Created by Sam DuBois on 7/16/18.
//  Copyright © 2018 Joshua Holme. All rights reserved.
//

import Foundation
import SpriteKit

// This class creates a model to illustrate the attributes of characters in the game as well as define common variables between all characters
class Player: SKSpriteNode {
    
    var PlayerJumpCount = 0
    var Photo: UIImage!
    var PlayerXScale: CGFloat!
    var PlayerYScale: CGFloat!
    
    func setPlayerPhysics() {
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody?.categoryBitMask = PhysicsCategory.character
        self.physicsBody?.collisionBitMask = PhysicsCategory.ground
        self.physicsBody?.contactTestBitMask = PhysicsCategory.ground
        self.physicsBody?.affectedByGravity = true
        self.physicsBody?.isDynamic = true
    }
    
    func addPlayertoScene(scene: SKScene) {
        scene.addChild(self)
    }
    
}
