//
//  Actions.swift
//  Rumble
//
//  Created by Sam DuBois on 7/17/18.
//  Copyright © 2018 Joshua Holme. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene {
    
    func move(value: Float, player: Player)
    {
        let moveBy = SKAction.moveBy(x: CGFloat(value * 20), y: 0, duration: 0.1)
        let moveByForever = SKAction.repeatForever(moveBy)
        player.run(moveByForever, withKey: "move")
    }
    
    func jump(player: Player)
    {
        if (player.PlayerJumpCount < 2)
        {
            player.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 1250))
            
            player.PlayerJumpCount += 1
        }
    }
    
    
}
