//
//  Controls.swift
//  Rumble
//
//  Created by Sam DuBois on 7/16/18.
//  Copyright © 2018 Joshua Holme. All rights reserved.
//

import Foundation
import GameController
import SpriteKit

extension GameScene {
    
    func performActions(gamepad: GCExtendedGamepad, element: GCControllerElement, player: Player) {
        
        // Left Thumbstick
        if (gamepad.leftThumbstick == element)
        {
            if (gamepad.leftThumbstick.xAxis.value != 0)
            {
                print("Pressed Left Thumbstick Left")
                self.move(value: gamepad.leftThumbstick.xAxis.value, player: player)
            }
            else if (gamepad.leftThumbstick.xAxis.value == 0)
            {
                player.removeAction(forKey: "move")
            }
        }
            
            // D-Pad
        else if (gamepad.dpad == element)
        {
            // Right
            if (gamepad.dpad.xAxis.value != 0)
            {
                print("Pressed D-Pad")
                self.move(value: gamepad.dpad.xAxis.value, player: player)
            }
                
                // Right
            else if (gamepad.dpad.xAxis.value == 0)
            {
                print("Released D-Pad")
                player.removeAction(forKey: "move")
            }
        }
            
            // A-Button
        else if (gamepad.buttonA == element)
        {
            // A
            if (gamepad.buttonA.value == 1)
            {
                self.jump(player: player)
                print("Pressed A Button")
            }
        }
    }
}
