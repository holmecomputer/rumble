//
//  GameControllerUtilities.swift
//  Rumble
//
//  Created by Sam DuBois on 7/16/18.
//  Copyright © 2018 Joshua Holme. All rights reserved.
//

import Foundation
import GameController
import SpriteKit

extension GameScene {
    
    // Function to run intially to lookout for any MFI or Remote Controllers in the area
    func ObserveForGameControllers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(connectControllers), name: NSNotification.Name.GCControllerDidConnect, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(disconnectControllers), name: NSNotification.Name.GCControllerDidDisconnect, object: nil)
    }
    
    // This Function is called when a controller is connected to the Apple TV
    @objc func connectControllers() {
        //Unpause the Game if it is currently paused
        self.isPaused = false
        
        //Used to register the Nimbus Controllers to a specific Player Number
        var indexNumber = 0
        
        // Run through each controller currently connected to the system
        for controller in GCController.controllers() {
            
            //Check to see whether it is an extended Game Controller (Such as a Nimbus)
            if controller.extendedGamepad != nil {
                
                controller.playerIndex = GCControllerPlayerIndex.init(rawValue: indexNumber)!
                
                indexNumber += 1
                
                setupControllerControls(controller: controller)
            }
        }
    }
    
    // Function called when a controller is disconnected from the Apple TV
    @objc func disconnectControllers() {
        // Pause the Game if a controller is disconnected ~ This is mandated by Apple
        self.isPaused = true
    }
    
    func setupControllerControls(controller: GCController) {
        
        //Function that check the controller when anything is moved or pressed on it
        controller.extendedGamepad?.valueChangedHandler = {
            (gamepad: GCExtendedGamepad, element: GCControllerElement) in
            
            // Add movement in here for sprites of the controllers
            let player = self.determineControllersSpriteCharacter(indexNumber: controller.playerIndex.rawValue)
            self.performActions(gamepad: gamepad, element: element, player: player)
            
        }
    }
    
//     Function used to determine the Sprite of the connected controller
    func determineControllersSpriteCharacter(indexNumber: Int) -> Player {

        if indexNumber == 0 {
            return self.player1
        } else if indexNumber == 1 {
            return self.player2
        } else if indexNumber == 2 {
            return self.player3
        } else { // if indexNumber == 3
            return self.player4
        }
    }
}
