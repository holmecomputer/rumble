//
//  GameScene.swift
//  Rumble
//
//  Created by Joshua Holme on 6/12/18.
//  Copyright © 2018 Joshua Holme. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameController

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var player1: Player = Profiles.instance.Crystal()
    var player2: Player = Profiles.instance.MoneyBags()
    var player3: Player = Profiles.instance.Astro()
    var player4: Player = Profiles.instance.Astro()
    
    let ground = SKSpriteNode(color: .red, size: CGSize(width: 50, height: 50))
    let leftPlatform = SKSpriteNode(color: .red, size: CGSize(width: 275, height: 25))
    let rightPlatform = SKSpriteNode(color: .red, size: CGSize(width: 275, height: 25))
    
    func didBegin(_ contact: SKPhysicsContact)
    {
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        
        
        if ((firstBody.categoryBitMask == PhysicsCategory.character && secondBody.categoryBitMask == PhysicsCategory.ground) || (firstBody.categoryBitMask == PhysicsCategory.ground && secondBody.categoryBitMask == PhysicsCategory.character))
        {
            // Reset Player Count as they approach the ground
        }
    }
    
    override func sceneDidLoad()
    {
        super.sceneDidLoad()
        
        setupScene()
        
        self.physicsWorld.contactDelegate = self
        
        // Ground Physics Properties
        ground.physicsBody = SKPhysicsBody(rectangleOf: ground.size)
        ground.physicsBody?.categoryBitMask = PhysicsCategory.ground
        ground.physicsBody?.collisionBitMask = PhysicsCategory.character
        ground.physicsBody?.contactTestBitMask = PhysicsCategory.character
        ground.physicsBody?.affectedByGravity = false
        ground.physicsBody?.isDynamic = false
        
        leftPlatform.physicsBody = SKPhysicsBody(rectangleOf: leftPlatform.size)
        leftPlatform.physicsBody?.categoryBitMask = PhysicsCategory.ground
        leftPlatform.physicsBody?.collisionBitMask = PhysicsCategory.character
        leftPlatform.physicsBody?.contactTestBitMask = PhysicsCategory.character
        leftPlatform.physicsBody?.affectedByGravity = false
        leftPlatform.physicsBody?.isDynamic = false
        
        rightPlatform.physicsBody = SKPhysicsBody(rectangleOf: rightPlatform.size)
        rightPlatform.physicsBody?.categoryBitMask = PhysicsCategory.ground
        rightPlatform.physicsBody?.collisionBitMask = PhysicsCategory.character
        rightPlatform.physicsBody?.contactTestBitMask = PhysicsCategory.character
        rightPlatform.physicsBody?.affectedByGravity = false
        rightPlatform.physicsBody?.isDynamic = false
        
        
        // Player Physics Properties
        player1.setPlayerPhysics()
        player2.setPlayerPhysics()
        player3.setPlayerPhysics()
    }
    
    func setupScene()
    {
        // Ground Properties
        ground.size.width = CGFloat(self.frame.width)
        ground.size.height = 50
        ground.position = CGPoint(x: 0.0, y: -(self.frame.height / 2))
        self.addChild(ground)
        
        leftPlatform.position = CGPoint(x: -(self.frame.width / 4), y: 50)
        self.addChild(leftPlatform)
        
        rightPlatform.position = CGPoint(x: (self.frame.width / 4), y: -50)
        self.addChild(rightPlatform)
        
        
        // Player Properties
        player1.addPlayertoScene(scene: self)
        player1.xScale = player1.PlayerXScale
        player1.yScale = player1.PlayerYScale
        player2.addPlayertoScene(scene: self)
        player2.xScale = player2.PlayerXScale
        player2.yScale = player2.PlayerYScale
        player3.addPlayertoScene(scene: self)
        player3.xScale = player3.PlayerXScale
        player3.yScale = player3.PlayerYScale
    }
    
    override func didMove(to view: SKView)
    {
        ObserveForGameControllers()
    }
}
